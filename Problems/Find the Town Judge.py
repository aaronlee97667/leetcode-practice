def findJudge(n, trust):
    reputation = {}
    not_judge = []

    if len(trust) == 0 and n == 1:
        return 1

    for trust_list in trust:
        if trust_list[1] in reputation:
            reputation[trust_list[1]] = reputation.get(trust_list[1]) + 1
            not_judge.append(trust_list[0])
        else:
            reputation[trust_list[1]] = 1
            not_judge.append(trust_list[0])

    not_judge = list({*not_judge})
    for key in reputation:
        if reputation.get(key) == n - 1 and key not in not_judge:
            return key

    return -1

n = 3
trust = [[1,3],[2,3]]

print(findJudge(n, trust))
