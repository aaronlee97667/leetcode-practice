def isValid(s):
    stack = []
    hashmap = {"}":"{", ")":"(", "]":"["}

    if len(s) % 2 == 1 or len(s) == 0:
        return False
    for char in s:
        if char in hashmap:
            if stack and stack[-1] == hashmap[char]:
                stack.pop()
            else:
                return False
        else:
            stack.append(char)
    return not stack

test = "]{}()"
print(isValid(test))
