# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head):
        temp = head

        while temp:
            if temp.next == None:
                return False

            if temp.val is None:
                return True
            else:
                temp.val = None
                temp = temp.next

