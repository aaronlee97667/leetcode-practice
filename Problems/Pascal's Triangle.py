def generate(numRows: int):
    if numRows == 0:
        return []

    output = [[1]]

    if numRows == 1:
        return output

    for i in range(numRows - 1):
        if i == 0:
            output.append([1,1])
        else:
            middle = []
            for idx in range(len(output[i]) - 1):
                middle.append(output[i][idx] + output[i][idx+1])
            output.append([1, *middle, 1])

    return output

print(generate(5)) # Expecting [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
