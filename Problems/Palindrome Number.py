def isPalindrome(x):
    reversed_string = str(x)[::-1]
    return str(x) == reversed_string

x = -121
print(isPalindrome(x)) # False
x = 121
print(isPalindrome(x)) # True
