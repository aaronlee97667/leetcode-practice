def wordPattern(pattern, s):
    hashmap = {}
    hashmap2 = {}
    new_string = s.split(" ")

    if len(new_string) != len(pattern):
        return False

    for i in range(len(pattern)):
        if pattern[i] not in hashmap:
            hashmap[pattern[i]] = i
        else:
            hashmap.update({pattern[i]:[hashmap[pattern[i]], i]})
        if new_string[i] not in hashmap2:
            hashmap2[new_string[i]] = i
        else:
            hashmap2.update({new_string[i]:[hashmap2[new_string[i]], i]})

    for value in hashmap.values():
        if value not in hashmap2.values():
            return False
    return True

pattern = "abba"
s = "dog cat cat dog"
print(wordPattern(pattern, s))
