def maximumWealth(accounts):
    max_wealth = 0

    for account in accounts:
        if (sum(account) > max_wealth):
            max_wealth = sum(account)

    return max_wealth

print(maximumWealth([[1,5],[7,3],[3,5]])) # Expected: 10

print(maximumWealth([[1,2,3],[3,2,1]])) # Expected: 6
