def longestNiceSubstring(s):
    longest_substring = ""

    if s == 0:
        return longest_substring

    hashmap = {}
    for i in range(len(s)):
        for j in range(i+1, len(s)+1):
            tmp = s[i:j]
            if tmp not in hashmap:
                hashmap[s[i:j]] = 1

    for key in hashmap.keys():
        for idx, letter in enumerate(key):
            if( (letter.upper() not in key) or (letter.lower() not in key) ):
                break
            if(idx == len(key)-1):
                if (len(key) > len(longest_substring)):
                    longest_substring = key

    return longest_substring

s = "YazaAay"
print(longestNiceSubstring(s)) # Expected: "aAa"

s = "Bb"
print(longestNiceSubstring(s)) # Expected: "Bb"
