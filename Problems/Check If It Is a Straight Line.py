def checkStraightLine(coordinates):
    # A function will be a straight line if the rate of growth is constant (y/x)
    # horizontal will have a constant y value
    # vertical will have a constant x value
    graph_state = ""
    rate_of_change = 0

    for i in range(len(coordinates) - 1):
        dy = coordinates[i+1][-1] - coordinates[i][-1]
        dx = coordinates[i+1][0] - coordinates[i][0]
        if i == 0:
            if dy == 0:
                graph_state = "horizontal"
            elif dx == 0:
                graph_state = "vertical"
            else:
                rate_of_change = dy/dx
                graph_state = "diagonal"
        if graph_state == "horizontal":
            if dy != 0:
                return False
        elif graph_state == "vertical":
            if dx != 0:
                return False
        else:
            if dx == 0:
                return False
            if rate_of_change != dy/dx:
                return False
            rate_of_change == dy/dx
    return True

coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]] # True
print(checkStraightLine(coordinates))
coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]] # False
print(checkStraightLine(coordinates))
