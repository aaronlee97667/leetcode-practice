def numJewelsInStones(jewels, stones):

    hashmap = {}
    output = 0

    for i in range(len(jewels)):
        hashmap[jewels[i]] = stones.count(jewels[i])

    for value in hashmap.values():
        output += value

    return output

jewels = "aA"
stones = "aAAbbbb"

print(numJewelsInStones(jewels, stones))
