def search(nums, target):
    low = 0
    high = len(nums) - 1
    while low <= high:
        middle = (low + high) // 2
        if nums[middle] == target:
            return middle
        elif target in nums[middle:]:
            low = middle + 1
        else:
            high = middle - 1
    return -1

test = [-1,0,3,5,9,12]
target = 9
print(search(test, target))
